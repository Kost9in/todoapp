
'use strict';

(function () {
  angular.module('todoApp').factory('todoData', ['$timeout', todoData]);

  function todoData($timeout) {
    var data = [
      [
        {
          name: 'Buy bread',
          checked: false
        },
        {
          name: 'Buy milk',
          checked: true
        },
        {
          name: 'Buy sugar',
          checked: false
        }
      ],
      [
        {
          name: 'Wash dishes',
          checked: true
        },
        {
          name: 'Call mom',
          checked: false
        }
      ]
    ];

    return {

      board: {
        get: function () {
          return $timeout(function () {
            return data;
          }, 300);
        },
        add: function () {
          return $timeout(function () {
            if (data.length < 3) data.push([]);
          }, 300);
        },
        remove: function (board) {
          return $timeout(function () {
            if (typeof board !== 'undefined') {
              data.splice(board, 1);
            }
          }, 300);
        }
      },

      todo: {
        add: function (board, name) {
          return $timeout(function () {
            if (typeof board !== 'undefined' && name) data[board].push({name: name, checked: false});
          }, 300);
        },
        save: function (todoItem) {
          return $timeout(function () {
            if (typeof todoItem === 'object' && todoItem.newName) data[todoItem.board][todoItem.itemId].name = todoItem.newName;
            return data[todoItem.board][todoItem.itemId];
          }, 300);
        },
        remove: function (item) {
          return $timeout(function () {
            data[item.board].splice(item.itemId, 1);
          }, 300);
        }
      }

    };
  }
})();
