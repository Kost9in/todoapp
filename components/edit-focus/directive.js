
'use strict';

(function () {
  function editFocus($timeout) {
    return {
      restrict: 'A',
      link: function ($scope, element, attrs) {
        attrs.$observe('editFocus', function (edit) {
          if (edit) {
            $timeout(function () {
              var input = element[0];
              var value = input.value;
              input.focus();
              input.value = '';
              input.value = value;
            }, 10);
          }
        });
      }
    };
  }

  angular.module('todoApp').directive('editFocus', ['$timeout', editFocus]);
})();
