
'use strict';

(function () {
  function todoItem($timeout, todoData) {
    var self = this;

    self.$timeout = $timeout;
    self.todoData = todoData;
    self.editing = 0;
    self.newName = '';
  }

  todoItem.prototype.edit = function () {
    var self = this;

    self.editing = 1;
    self.newName = self.item.name;
  };

  todoItem.prototype.checked = function() {
    var self = this;

    self.todoData.todo.save(self);
  };

  todoItem.prototype.save = function() {
    var self = this;

    self.todoData.todo.save(self).then(function () {
      self.editing = 0;
      self.newName = '';
    });
  };

  todoItem.prototype.remove = function() {
    var self = this;
    self.todoData.todo.remove(self);
  };

  angular.module('todoApp').controller('todoItem', ['$timeout', 'todoData', todoItem]);
})();
