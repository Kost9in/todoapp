
'use strict';

(function () {
  function todoItem() {
    return {
      restrict: 'E',
      templateUrl: './components/todo-item/template.html',
      controllerAs: 'ctrl',
      controller: 'todoItem',
      bindToController: true,
      scope: {
        itemId: '=',
        item: '=',
        board: '='
      }
    };
  }

  angular.module('todoApp').directive('todoItem', todoItem);
})();
