
'use strict';

(function () {
  function boardItem(todoData) {
    var self = this;

    self.todoData = todoData;
    self.FILTER_VALUES = {
      all: {key: 'all', value: 'All'},
      progress: {key: 'progress', value: 'In progress'},
      finish: {key: 'finish', value: 'Finish'}
    };
    self.filter = 'all';
  }

  boardItem.prototype.remove = function () {
    var self = this;

    self.todoData.board.remove(self.boardId);
  };

  angular.module('todoApp').controller('boardItem', ['todoData', boardItem]);
})();
