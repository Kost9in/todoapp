
'use strict';

(function () {
  function boardItem() {
    return {
      restrict: 'E',
      templateUrl: './components/board-item/template.html',
      controllerAs: 'ctrl',
      controller: 'boardItem',
      bindToController: true,
      scope: {
        boardId: '=',
        list: '=board'
      }
    };
  }

  angular.module('todoApp').directive('boardItem', boardItem);
})();
