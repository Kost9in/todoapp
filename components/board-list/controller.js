
'use strict';

(function () {
  function boardList(todoData) {
    var self = this;

    self.boards = [];
    self.todoData = todoData;

    todoData.board.get().then(function (boards) {
      self.boards = boards;
    });
  }

  boardList.prototype.add = function () {
    var self = this;

    self.todoData.board.add();
  };

  angular.module('todoApp').controller('boardList', ['todoData', boardList]);
})();
