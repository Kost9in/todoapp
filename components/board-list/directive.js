
'use strict';

(function () {
  function boardList() {
    return {
      restrict: 'E',
      templateUrl: './components/board-list/template.html',
      controllerAs: 'ctrl',
      controller: 'boardList',
      bindToController: true
    };
  }

  angular.module('todoApp').directive('boardList', boardList);
})();
