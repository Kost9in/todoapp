
'use strict';

(function () {
  function todoAdd() {
    return {
      restrict: 'E',
      templateUrl: './components/todo-add/template.html',
      controllerAs: 'ctrl',
      controller: 'todoAdd',
      bindToController: true,
      scope: {
        board: '='
      }
    };
  }

  angular.module('todoApp').directive('todoAdd', todoAdd);
})();
