
'use strict';

(function () {
  function todoAdd(todoData) {
    var self = this;

    self.todoData = todoData;
    self.newName = '';
  }

  todoAdd.prototype.add = function () {
    var self = this;

    self.todoData.todo.add(self.board, self.newName).then(function () {
      self.newName = '';
    });
  };

  angular.module('todoApp').controller('todoAdd', ['todoData', todoAdd]);
})();
